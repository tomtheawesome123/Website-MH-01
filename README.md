# Website-MH-01

This is my website labeled as Website-MH-01 originally started with this website on 6-6-2020.
The earliest I started with website(s) was on 6-5-2020.

The main Shortened URL is:
"https://tiny.cc/WMH01"

That takes you here:
[Website link/URL](https://mhmatthewhugley.gitlab.io/Website-MH-01/),
"https://mhmatthewhugley.gitlab.io/Website-MH-01/".

[Code Of Conduct](./CODE_OF_CONDUCT.md).

Issue Templates [Link](./.github/ISSUE_TEMPLATE).

Information on the LICENSE(s) and what is under each LICENSE is in the [Notice/Information NOTICE.txt](./public/NOTICE.txt) file.

Old/Current Website URL is: "https://www.otgt.us.eu.org"